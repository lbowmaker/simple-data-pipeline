# simple-data-pipeline

This repo contains a simple notebook that queries analytics tables to find the number of pages created on a given day.

The aim of the repo is to provide a simple example of a Platform Airflow DAG that can be copied and built upon.
